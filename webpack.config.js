const path = require('path'),
      ExtractTextPlugin = require('extract-text-webpack-plugin'),
      autoprefixer = require('autoprefixer'),
      HTMLWebpackPlugin = require('html-webpack-plugin');

module.exports = {

  entry: './index.js',

  module: {

    rules: [
      {
        test: /\.js?$/,
        include: path.resolve(__dirname, 'src'),
        loader: 'babel-loader'
      },

      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                sourceMap: true
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                plugins: [autoprefixer('last 2 version')],
                sourceMap: true
              }
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true
              }
            }
          ]
        })
      },

      {
        test: /\.html$/,
        loader: 'underscore-template-loader'
      }
    ]
  },

  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendors",
          chunks: "all"
        }
      }
    }
  },

  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].bundle.js'
  },

  plugins: [

    new ExtractTextPlugin({
      filename: 'css/styles.css',
      allChunks: false
    }),

    new HTMLWebpackPlugin({
      template: path.join(__dirname, 'index.html'),
      filename: 'index.html',
      inject: 'body'
    })

  ],

  devtool: 'source-map',

  watchOptions: {
    ignored: /node_modules/
  },

  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true
  }

};
