import './src/Application.scss';
import Application from './src/Application';

document.addEventListener('DOMContentLoaded', () => {
  const app = new Application();
  app.start();
});
