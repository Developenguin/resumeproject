export default {

  personalia: {
    name: 'Matthijs Rijken',
    title: 'Msc.',
    jobTitle: 'Frontend Developer',
    summary: '',
    city: 'Vleuten',
    dateOfBirth: '1992-09-30',
    linkedinUrl: 'https://linkedin.com/in/mrijken'
  },

  experience: [ {
    title: 'Frontend Developer',
    place: 'Medicore',
    startDate: '01-06-2016',
    endDate: null,
    description: 'Als frontend developer houd ik me bezig met de ontwikkeling van de Medicore producten. Deze applicaties maken de zorg makkelijker, efficiënter en beter te begrijpen voor zorgverleners en patiënten.'
  }, {
    title: 'Web consultant',
    place: 'CCS B.V.',
    startDate: '14-11-2014',
    endDate: '31-05-2016',
    description: 'Web consultancy omvatte het implementeren en configureren van Connect, een frontoffice-applicatie die verzekeraars, tussenpersonen en consumenten hun verzekeringen en schades online laat administreren en beheren.'
  }, {
    title: 'MSc. Thesis',
    place: 'ZEEF',
    startDate: '01-04-2014',
    endDate: '31-07-2014',
    description: 'Ik heb mijn Master thesis bij ZEEF geschreven. Mijn onderzoek richtte zich op het combineren van van zoekalgoritmes en content curation, en de toegevoegde waarde hiervan voor eindgebruikers.'
  }, {
    title: 'Studentassistent',
    place: 'Universiteit Utrecht',
    startDate: '01-02-2013',
    endDate: '30-04-2013',
    description: 'Voor het bachelorvak Game-ontwerp ben ik studentassistent geweest. Mijn werkzaamheden waren het nakijken van tentamens, beoordelen van opdrachten en vragen van studenten beantwoorden.'
  } ],

  education: [ {
    title: 'MSc. Human Centered Multimedia',
    place: 'Universiteit van Amsterdam',
    startDate: '01-09-2013',
    endDate: '31-07-2014',
    note: 'cum laude',
    description: 'HCM richt zich op hoe mensen technologie zien en gebruiken, en hoe ze erdoor worden beïnvloed. Focusgebieden zijn onder andere het ontwerpen van interactieve systemen, (visueel) zoeken en kennismanagement'
  }, {
    title: 'BSc. Informatiekunde',
    place: 'Universiteit Utrecht',
    startDate: '01-09-2009',
    endDate: '28-02-2013',
    description: 'Informatiekunde is de link tussen de wetenschap, IT, het bedrijfsleven en mensen. Mijn focus tijdens mijn studie lag op cognitie, user experience engineering, gebruiksvriendelijke systemen ontwerpen en het softwareontwikkelproces'
  }, {
    title: 'VWO N&G (oude stijl)',
    place: 'Pallas Athene College, Ede',
    startDate: '01-09-2004',
    endDate: '31-07-2009',
    description: 'Ik heb het VWO in vijf jaar afgerond, een optie die mijn school in 2004 voor het eerst aanbood.'
  } ],

  skills: {
    technical: [ 'Javascript', 'React', 'Backbone', 'Marionette', '(S)CSS' ],
    languages: [ 'Nederlands', 'Engels' ],
    personal: [ 'Communicatief', 'Gedreven', 'Betrouwbaar' ]
  }

};

// ResumeItem
//{
//  title: '',
//  place: '',
//  startDate: '',
//  endDate: '',
//  description: ''
//}
