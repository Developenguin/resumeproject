import { Model } from 'backbone';

export default Model.extend({

  defaults: {
    id: null,
    name: null,
    title: null,
    jobTitle: null,
    summary: null,
    city: null,
    dateOfBirth: null,
    linkedinUrl: null
  }

});
