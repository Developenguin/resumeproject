import { Collection } from 'backbone';
import ResumeItemModel from './ResumeItemModel';

export default Collection.extend({
  model: ResumeItemModel
});
