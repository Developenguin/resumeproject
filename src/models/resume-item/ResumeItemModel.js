import { Model } from 'backbone';

export default Model.extend({

  defaults: {
    id: null,
    title: null,
    place: null,
    description: null,
    startDate: null,
    endDate: null,
    note: null
  }

});
