import { Application } from 'backbone.marionette';
import ResumeLayout from './components/ResumeLayout/ResumeLayoutView';

export default Application.extend({

  region: '#app',

  onStart() {
    this.showView(new ResumeLayout());
  }

});
