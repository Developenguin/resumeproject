import data from './assets/data/data';
import PersonaliaModel from './models/personalia/PersonaliaModel';
import ResumeItemCollection from './models/resume-item/ResumeItemCollection';

export default {

  getPersonaliaModel() {
    return new PersonaliaModel(data.personalia);
  },

  getExperienceCollection() {
    return new ResumeItemCollection(data.experience);
  },

  getEducationCollection() {
    return new ResumeItemCollection(data.education);
  },

  getSkills() {
    return data.skills;
  }

};
