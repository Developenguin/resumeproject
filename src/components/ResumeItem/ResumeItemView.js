import { View } from 'backbone.marionette';
import template from './resume-item.html';

export default View.extend({

  className: 'resume-item pb-4 px-4',
  template

});

