import { View } from 'backbone.marionette';
import template from './hexagon-list-item.html';
import HexagonView from '../../Hexagon/HexagonView';
import HexagonViewModel from '../../Hexagon/HexagonViewModel';
import ColorUtil from '../../../assets/util/ColorUtil';

export default View.extend({

  className: 'col-12 d-flex align-items-center hexagon-list-item',
  template,

  regions: {
    hexagonRegion: '[data-region="item-hexagon"]'
  },

  onRender() {
    this.showChildView('hexagonRegion', new HexagonView({
      model: new HexagonViewModel({
        icon: this.model.get('icon'),
        borderColor: ColorUtil.textOnPrimaryBackgroundColor,
        backgroundColor: ColorUtil.primaryColor,
        faIconFamily: this.model.get('faIconFamily')
      })
    }));
  }

});
