import { CollectionView } from 'backbone.marionette';
import HexagonListItemView from './HexagonListItem/HexagonListItemView';

export default CollectionView.extend({

  className: 'row hexagon-list',
  childView: HexagonListItemView

});
