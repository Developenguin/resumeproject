import { View } from 'backbone.marionette';
import template from './hexagon.html';
import HexagonViewModel from './HexagonViewModel';
import ColorUtil from '../../assets/util/ColorUtil';

export default View.extend({

  className: 'hexagon',
  template,

  initialize(options) {
    this.model = options.model || new HexagonViewModel();
  },

  onRender() {

    const backgroundColor = this.model.get('backgroundColor'),
          borderColor = this.model.get('borderColor'),
          hexOuter = this.$el.find('.hexagon-outer'),
          hexInner = this.$el.find('.hexagon-inner'),
          hexIcon = this.$el.find('.hexagon-icon');

    if (this.model.get('backgroundColor')) {
      hexInner.css('background-color', backgroundColor);
    }

    if (this.model.hasBorder()) {
      hexOuter.removeClass('hidden');
      hexOuter.css('background-color', borderColor);
    }

    if (this.model.hasIcon()) {
      hexIcon.removeClass('hidden');
      hexIcon.addClass(`fa-${this.model.get('icon')}`);
    }

    if (this.model.hasIconColor()) {
      hexIcon.css({
        color: this.model.get('iconColor')
      });
    } else {
      hexIcon.css({
        color: ColorUtil.textOnPrimaryBackgroundColor
      });
    }

    hexIcon.addClass(`${this.model.get('faIconFamily') || 'fas'}`);

  }

});
