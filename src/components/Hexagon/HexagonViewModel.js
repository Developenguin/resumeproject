import { Model } from 'backbone';

export default Model.extend({

  defaults: {
    id: null,
    backgroundColor: null,
    borderColor: null,
    icon: null,
    iconColor: null,
    faIconFamily: null
  },

  hasBorder() {
    return !!this.get('borderColor');
  },

  hasIcon() {
    return !!this.get('icon');
  },

  hasIconColor() {
    return !!this.get('iconColor');
  }

});
