import { View } from 'backbone.marionette';
import template from './sidebar.html';
import HexagonListView from '../HexagonList/HexagonListView';
import { Collection } from 'backbone';
import moment from 'moment';
import 'moment/locale/nl';
import DataService from '../../DataService';
import ColorUtil from '../../assets/util/ColorUtil';

export default View.extend({

  // Keep track of whether the sidebar is shown on smaller screens
  isExpanded: false,

  id: 'sidebar',
  className: 'p-4 pt-md-3 pt-lg-4 h-100',
  template,

  events: {
    'click .sidebar-switch': 'onClickSidebarSwitch'
  },

  regions: {
    contactRegion: '[data-region="contact"]'
  },

  ui: {
    sidebarRow: '.sidebar-row'
  },

  initialize() {
    this.model = DataService.getPersonaliaModel();
    moment.locale('nl');
  },

  onRender() {

    const dateOfBirth = this.model.get('dateOfBirth');

    this.showChildView('contactRegion', new HexagonListView({
      collection: new Collection([
        { text: moment(dateOfBirth).format('DD MMMM'), icon: 'user' },
        { text: this.model.get('city'), icon: 'home' },
        { text: this.model.get('linkedinUrl'), icon: 'linkedin', faIconFamily: 'fab' }
      ])
    }));

    this.setColorsOnSidebar();

  },

  onClickSidebarSwitch() {

    if (!this.isExpanded) {
      this.showSidebar();
    } else {
      this.hideSidebar();
    }

  },

  setColorsOnSidebar() {

    this.$el.css({
      background: ColorUtil.primaryColor,
      color: ColorUtil.textOnPrimaryBackgroundColor
    });

    this.$el.find('.fa-menu').css({
      color: ColorUtil.textOnPrimaryBackgroundColor
    });

  },

  showSidebar() {

    this.isExpanded = true;

    this.ui.sidebarRow.removeClass('d-none');
    this.$el.parent().removeClass('col-1');
    this.$el.parent().addClass('col-6');
  },

  hideSidebar() {

    this.isExpanded = false;

    this.ui.sidebarRow.addClass('d-none');
    this.$el.parent().addClass('col-1');
    this.$el.parent().removeClass('col-6');
  }

});
