import { View } from 'backbone.marionette';
import template from './experience-layout.html';
import HexagonView from '../../Hexagon/HexagonView';
import HexagonViewModel from '../../Hexagon/HexagonViewModel';
import ExperienceListView from './ExperienceList/ExperienceListView';
import ColorUtil from '../../../assets/util/ColorUtil';

export default View.extend({

  id: 'experience',

  template,

  regions: {
    hexagonRegion: '[data-region="experience-hexagon"]',
    listRegion: '[data-region="experience-list"]'
  },

  onRender() {

    this.showChildView('hexagonRegion', new HexagonView({
      model: new HexagonViewModel({
        backgroundColor: ColorUtil.primaryColor,
        borderColor: ColorUtil.textOnPrimaryBackgroundColor,
        icon: 'user'
      })
    }));

    this.showChildView('listRegion', new ExperienceListView());

  }

});
