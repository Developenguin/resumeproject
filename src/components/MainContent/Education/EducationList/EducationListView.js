import { CollectionView } from 'backbone.marionette';
import ResumeItemView from '../../../ResumeItem/ResumeItemView';
import DataService from '../../../../DataService';

export default CollectionView.extend({

  id: 'education-list',

  childView: ResumeItemView,

  initialize() {
    this.collection = DataService.getEducationCollection();
  }

});
