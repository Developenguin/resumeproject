import { View } from 'backbone.marionette';
import template from './education-layout.html';
import HexagonView from '../../Hexagon/HexagonView';
import HexagonViewModel from '../../Hexagon/HexagonViewModel';
import EducationListView from './EducationList/EducationListView';
import ColorUtil from '../../../assets/util/ColorUtil';

export default View.extend({

  id: 'education',

  template,

  regions: {
    hexagonRegion: '[data-region="education-hexagon"]',
    listRegion: '[data-region="education-list"]'
  },

  onRender() {

    this.showChildView('hexagonRegion', new HexagonView({
      model: new HexagonViewModel({
        backgroundColor: ColorUtil.primaryColor,
        borderColor: ColorUtil.textOnPrimaryBackgroundColor,
        icon: 'graduation-cap'
      })
    }));

    this.showChildView('listRegion', new EducationListView());

  }

});
