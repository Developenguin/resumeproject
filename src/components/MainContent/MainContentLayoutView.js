import { View } from 'backbone.marionette';
import template from './main-content.html';
import ExperienceLayoutView from './Experience/ExperienceLayoutView';
import EducationLayoutView from './Education/EducationLayoutView';
import SkillsLayoutView from './Skills/SkillsLayoutView';

export default View.extend({
  template,

  regions: {
    experienceRegion: '[data-region="experience"]',
    educationRegion: '[data-region="education"]',
    skillsRegion: '[data-region="skills"]'
  },

  onRender() {
    this.showChildView('experienceRegion', new ExperienceLayoutView());
    this.showChildView('educationRegion', new EducationLayoutView());
    this.showChildView('skillsRegion', new SkillsLayoutView());
  }
});
