import { View } from 'backbone.marionette';
import template from './skills-layout.html';
import HexagonViewModel from '../../Hexagon/HexagonViewModel';
import HexagonView from '../../Hexagon/HexagonView';
import { Collection } from 'backbone';
import HexagonListView from '../../HexagonList/HexagonListView';
import DataService from '../../../DataService';
import ColorUtil from '../../../assets/util/ColorUtil';

export default View.extend({

  id: 'skills',

  template,

  regions: {
    hexagonRegion: '[data-region="skills-hexagon"]',
    technicalHexagonRegion: '[data-region="technical-hexagon"]',
    languagesHexagonRegion: '[data-region="language-hexagon"]',
    personalHexagonRegion: '[data-region="personal-hexagon"]',

    technicalSkillsRegion: '[data-region="technical-skills-list"]',
    languagesSkillsRegion: '[data-region="language-skills-list"]',
    personalSkillsRegion: '[data-region="personal-skills-list"]'
  },

  onRender() {

    this.showChildView('hexagonRegion', new HexagonView({
      model: new HexagonViewModel({
        backgroundColor: ColorUtil.primaryColor,
        borderColor: ColorUtil.textOnPrimaryBackgroundColor,
        icon: 'cubes'
      })
    }));

    this.showSkillLists();

  },

  showSkillLists() {

    const subViewData = [
            { name: 'technical', icon: 'code' },
            { name: 'languages', icon: 'comments' },
            { name: 'personal', icon: 'handshake' }
          ],
          skills = DataService.getSkills();

    for (let data of subViewData) {

      this.showChildView(`${data.name}HexagonRegion`, new HexagonView({
        model: new HexagonViewModel({
          backgroundColor: ColorUtil.textOnPrimaryBackgroundColor,
          borderColor: ColorUtil.primaryColor,
          icon: data.icon,
          iconColor: ColorUtil.primaryColor
        })
      }));

      this.showChildView(`${data.name}SkillsRegion`, new HexagonListView({
        collection: new Collection(skills[data.name].map(skill => { return { text: skill }} ))
      }));

    }

  }

});
