import { View } from 'backbone.marionette';
import template from './resume-layout.html';
import SidebarLayout from '../Sidebar/SidebarLayoutView';
import MainContentLayout from '../MainContent/MainContentLayoutView';

export default View.extend({

  id: 'resume',
  template,

  regions: {
    sidebarRegion: '[data-region="sidebar"]',
    mainRegion: '[data-region="main-content"]'
  },

  onRender() {
    this.showSidebar();
    this.showMainContent();
  },

  showSidebar() {
    this.showChildView('sidebarRegion', new SidebarLayout());
  },

  showMainContent() {
    this.showChildView('mainRegion', new MainContentLayout());
  }
});
